import java.util.Scanner;


/**
 * Basic REPL for testing string lists.
 *
 * @author Michael Siff
 */
public class ListTester {
    public static void main(String[] args) {
        int pos;
        String s;
        Scanner scanner = new Scanner(System.in);
        StringListInterface list = new StringList();
        
        boolean replOn = true;
        while (replOn) {
            System.out.println("\nlist size: " + list.size());
            System.out.println("list contents: " + list);
            System.out.println(
                "\n" +
                "MENU\n" +
                "----\n" +
                "a)ppend an item\n" +
                "i)nsert an item\n" +
                "r)emove an item\n" +
                "c)lear all items\n" +
                "g)et item\n" +
                "s)et item\n" +
                "f)ind index of item\n" +
                "l)ook for item\n" +
                "q)uit\n");
                
            System.out.print("action: ");
            String input = scanner.next();
            char c = input.charAt(0);
            switch (c) {
                case 'q': {
                    replOn = false;
                    break;
                }
                case 'c': {
                    list.clear();
                    break;
                }
                case 'a': {
                    System.out.print("string to append: ");
                    s = scanner.next();
                    list.add(s);
                    break;
                }
                case 'i': {
                    System.out.print("position where to insert: ");
                    pos = scanner.nextInt();
                    if (pos >= 0 && pos < list.size()) {
                        System.out.print("string to insert: ");
                        s = scanner.next();
                        list.add(pos, s);
                    } else {
                        System.out.println("bad index");
                    }
                    break;
                }
                case 'r': {
                    System.out.print("position of item to remove: ");
                    pos = scanner.nextInt();
                    if (pos >= 0 && pos < list.size()) {
                        s = list.remove(pos);
                        System.out.println("item removed: " + s);
                    } else {
                        System.out.println("bad index");
                    }
                    break;
                }                     
                case 'g': {
                    System.out.print("position of item to get: ");
                    pos = scanner.nextInt();
                    if (pos >= 0 && pos < list.size()) {
                        s = list.get(pos);
                        System.out.println("item at position " + pos + " is " + s);
                    } else {
                        System.out.println("bad index");
                    }
                    break;
                }
                case 's': {
                    System.out.print("position to set: ");
                    pos = scanner.nextInt();
                    if (pos >= 0 && pos < list.size()) {
                        System.out.print("string to place there: ");
                        s = scanner.next();
                        list.set(pos, s);
                    } else {
                        System.out.println("bad index");
                    }                
                    break;
                }
                case 'f': {
                    System.out.print("item to find: ");
                    s = scanner.next();
                    pos = list.indexOf(s);
                    if (pos != -1) {
                        System.out.println("Found! It is as at position: " + pos);
                    } else {
                        System.out.println("Not found!");
                    }
                    break;
                }
                case 'l': {
                    System.out.print("item to check for: ");
                    s = scanner.next();
                    if (list.contains(s)) {
                        System.out.println("Found!");
                    } else {
                        System.out.println("Not found!");
                    }
                    break;
                }
            }       
        }
    }
}
