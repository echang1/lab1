/**
 * Lists of strings using resizable arrays.
 * 
 * @author Bill & Carlos
 */

public class StringList implements StringListInterface {
    private static final int INIT_CAP = 32;
    
    private String[] _array;
    private int _size;
    
    public StringList() {
        _array = new String[INIT_CAP];
        _size = 0;
    }
    
    public boolean isEmpty() {
        return _size == 0;
    }
    
    public int size() {
        return _size;
    }
    
    public String get(int index) {
        if (index >= 0 && index < _size) {
            return _array[index];
        } else {
            throw new RuntimeException("invalid list index: " + index);
        }
    }
    
    public void set(int index, String element) {
        if (index >= 0 && index < _size) {
            _array[index] = element;
        } else {
            throw new RuntimeException("invalid list index: " + index);
        }
    }
    
    public void add(int index, String element) {
        if (index >= 0 && index <= _size) {
            if (_size < _array.length) {
                for (int i = _size; i > index; i--) {
                    _array[i] = _array[i-1];
                }
                _array[index] = element;
                _size++;
            } else {
                throw new RuntimeException("list already at capacity");
            }
        } else {
            throw new RuntimeException("invalid list index: " + index);
        }
    }
    
    public void add(String element) {
        add(_size, element);
    }
    
    public boolean contains(String element) {
        return indexOf(element) != -1;
    }
    
    public int indexOf(String element) {
        int pos = -1;
        int i = 0;
        while (pos == -1 && i < _size) {
            if (_array[i].equals(element)) {
                pos = i;
            } else {
                i++;
            }
        }
        return pos;
    }
    
    public void clear() {
        _size = 0;
    }
    
    public String remove(int index) {
        if (index >= 0 && index < _size) {
            String element = _array[index];
            _size--;
            for (int i = index; i < _size; i++) {
                _array[i] = _array[i+1];
            }
            return element;
        } else {
            throw new RuntimeException("invalid list index: " + index);
        }
    }
    
    public String toString() {
        String s = "[";
        if (_size > 0) {
            s += _array[0];
            for (int i = 1; i < _size; i++) {
                s += ", ";
                s.concat(_array[i]);
            }
        }
        s += "]";
        return s;
    }

    
}
